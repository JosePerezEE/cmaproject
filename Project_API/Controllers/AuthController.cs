﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Project_API.Context.Models;
using Project_API.Core.AuthManager;
using Project_API.Dtos;

namespace Project_API.Controllers
{
    //Controller in which our API is created by calling a http request or POST so we can login into the program with our main credentials.
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        //Dependency injections
        private readonly IAuthManager _authManager;
        public AuthController(IAuthManager authManager)
        {
            _authManager = authManager;
        }

        //Code used to register the user and store it in the database
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel user)
        {
            //Validating the user
            user.UserName = user.UserName.ToLower();

            if (await _authManager.UserExist(user.UserName))
            {
                return BadRequest("Username already exists.");
            }
            if (user.Password != user.ConfirmPassword)
            {
                return BadRequest("Passwords do not match.");
            }

            var userToCreate = new User
            {
                UserName = user.UserName
            };
            //this is by using the register logic that was created in the AuthManager.
            var createdUser = await _authManager.Register(userToCreate, user.Password);
            return Created("Creado", createdUser);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserForLogin user)
        {
            var userFromRepo = await _authManager.Login(user.UserName.ToLower(), user.Password);

            if (userFromRepo == null)
            {
                return Unauthorized("Incorrect username or password.");
            }

            return Ok(new
            {
                token = _authManager.DoLogin(userFromRepo)
            });
        }
    }
}
