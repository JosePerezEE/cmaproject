﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Project_API.Context.Models;
using Project_API.Core.RecordManager;

namespace Project_API.Controllers
{
    //Controller in which the records displayed in the form are stored directly into the database through the API.
    [Route("api/[controller]")]
    [ApiController]
    public class RecordsController : ControllerBase
    {
        //Dependency injections
        private readonly IRecordManager _recordManager;
        public RecordsController(IRecordManager recordManager)
        {
            _recordManager = recordManager;
        }

        //HttpPost used to create the records
        [HttpPost]
        public async Task<ActionResult> Post(Record record)
        {
            await _recordManager.Create(record);
            return Created("",record);
        }
    }
}
