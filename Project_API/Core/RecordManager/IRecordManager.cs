﻿using Project_API.Context.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_API.Core.RecordManager
{
    //Method used in the RecordManager class
    public interface IRecordManager
    {
        Task Create(Record record);
    }
}
