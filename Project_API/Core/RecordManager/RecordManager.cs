﻿using Project_API.Context;
using Project_API.Context.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_API.Core.RecordManager
{
    public class RecordManager : IRecordManager
    {
        //Dependency injections
        private readonly CMADataContext _dataContext;
        public RecordManager(CMADataContext dataContext)
        {
            _dataContext = dataContext;
        }

        //Method used to add a specific record to the database by using Code first.
        public async Task Create(Record record)
        {
            try
            {
                await _dataContext.Records.AddAsync(record);
                await _dataContext.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
