﻿using Project_API.Context.Models;
using System.Threading.Tasks;

namespace Project_API.Core.AuthManager
{
    //Methods used in the AuthManager class
    public interface IAuthManager
    {
        Task<User> FindByUserName(string userName);
        Task<User> Register(User user, string password);
        Task<User> Login(string userName, string password);
        Task<bool> UserExist(string userName);
        string DoLogin(User user);
    }
}
