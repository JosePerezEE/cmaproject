﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Project_API.Context;
using Project_API.Context.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Project_API.Core.AuthManager
{
    public class AuthManager : IAuthManager
    {
        private readonly CMADataContext _context;
        private readonly IConfiguration _config;

        //Dependency injections
        public AuthManager(CMADataContext context, IConfiguration config)
        {
            this._context = context;
            _config = config;
        }

        //Code used to find a username stored in the database.
        public async Task<User> FindByUserName(string userName)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.UserName == userName);
        }

        //Code used specifically to create the Login logic 
        public async Task<User> Login(string userName, string password)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.UserName == userName);
            //validate if user exists.
            if (user == null)
            {
                return null;
            }
            //verfity the password hash.
            if (!VefiryPasswordHash(password, user.PasswordHash, user.PasswordSalt))
            {
                return null;
            }

            return user;
        }

        //Code used specifically to create the Register logic 
        public async Task<User> Register(User user, string password)
        {
            //creating the password hash and salt for extra security.
            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            //creating an specific user by storing it in the database.
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();

            return user;
        }

        //to specify if a user exists by returning a bool.
        public async Task<bool> UserExist(string userName)
        {
            if (await _context.Users.AnyAsync(s => s.UserName == userName))
                return true;

            return false;
        }

        //IMPORTANT
        //Code used to validate a user and generate an specific and unique token.
        //It returns a token.
        public string DoLogin(User userFromRepo)
        {
            var claims = new[]
           {
                new Claim(ClaimTypes.NameIdentifier, userFromRepo.Id.ToString()),
                new Claim(ClaimTypes.Name, userFromRepo.UserName)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8
            .GetBytes(_config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            //TokenDescriptor is the way the generated token is created.
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            //Creation of the token
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }

        //Password hash used in the register logic to add security to the whole process.
        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        //Code used to verify the password hash and salt.
        //It returns a bool.
        private bool VefiryPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i]) return false;
                }
            }
            return true;
        }
    }
}
