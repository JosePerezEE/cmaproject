﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Project_API.Dtos
{
    //Model created to be used during the login process.
    //It is a basic login, so it just includes a username and password.
    public class UserForLogin
    {
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
