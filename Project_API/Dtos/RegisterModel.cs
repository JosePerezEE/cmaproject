﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Project_API.Dtos
{
    //Specific model for the registering process, this is used in the Initializer or Seed.
    public class RegisterModel
    {
        //Each variable has a requirement to comply with the purpose of ensuring security, and following the Hash guidelines.
        
        [Required(ErrorMessage = "Username is required.")]
        public string UserName { get; set; }

        [Required]
        [StringLength(16, MinimumLength = 6, ErrorMessage = "The password length is between 4 to 16 characters.")]
        public string Password { get; set; }

        [Required]
        [StringLength(16, MinimumLength = 6, ErrorMessage = "The password length is between 4 to 16 characters.")]
        public string ConfirmPassword { get; set; }
    }
}
