﻿using Microsoft.EntityFrameworkCore;
using Project_API.Context.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_API.Context
{
    //Context by which datasets will be called throughout the entire coding of the program or API.
    //It also specifies the datasets that will be created.
    public class CMADataContext : DbContext
    {
        public CMADataContext(DbContextOptions<CMADataContext> options) 
            :base(options) {}

        public DbSet<User> Users { get; set; }
        public DbSet<Record> Records { get; set; }
    }
}
