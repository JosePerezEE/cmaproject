﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Project_API.Context.Models
{
    //Model for creating the records specified in the form.
    public class Record
    {
        public int Id { get; set; }
        //Foreign key to specify that this table is related with users.
        [ForeignKey("User")]
        public int UserId { get; set; }
        public bool Seal { get; set; }
        public bool Hazards { get; set; }
        public bool DamagesFound { get; set; }
        public string Note { get; set; }
        public virtual User User { get; set; }
    }
}
