﻿using Project_API.Context.Models;
using Project_API.Core.AuthManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_API.Context
{
    public class Initializer
    {
        //Unique helper that will create our seed including the first user to login into the program.
        public static async void SeedDatabase(CMADataContext context, IAuthManager authRepository)
        {
            await CreateUser(context, authRepository);
            await context.SaveChangesAsync();
        }
        //Seed used to create a user, meaning that this will be our main source of users while we lack a formal register page.
        //This is also the main user used to login into the program.
        private static async Task CreateUser(CMADataContext context, IAuthManager authRepository)
        {
            if (!context.Users.Any())
            {
                var user = new User
                {
                    UserName = "admin",
                };

                await authRepository.Register(user, "Qwerty1234@");
            }
        }
    }
}
