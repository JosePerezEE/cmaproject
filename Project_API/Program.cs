using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Project_API.Context;
using Project_API.Core.AuthManager;

namespace Project_API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            //Main method used to manage the migrations and the initializer along with the seed.
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<CMADataContext>();
                    var authManager = services.GetRequiredService<IAuthManager>();
                    context.Database.Migrate();
                    Initializer.SeedDatabase(context, authManager);
                    host.Run();
                }
                catch (Exception e)
                {
                    //Specify errors in the migration process.
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(e, "Error in the migration");
                }
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
