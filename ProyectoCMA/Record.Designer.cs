﻿namespace ProyectoCMA
{
    partial class Record
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Decrease = new System.Windows.Forms.Button();
            this.Increase = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Pause = new System.Windows.Forms.Button();
            this.Play = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.Seal = new System.Windows.Forms.CheckBox();
            this.Hazard = new System.Windows.Forms.CheckBox();
            this.Damage = new System.Windows.Forms.CheckBox();
            this.Notes = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Complete = new System.Windows.Forms.Button();
            this.ErrorMessage = new System.Windows.Forms.Label();
            this.exit = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Decrease
            // 
            this.Decrease.BackColor = System.Drawing.Color.Transparent;
            this.Decrease.FlatAppearance.BorderSize = 0;
            this.Decrease.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Decrease.Location = new System.Drawing.Point(3, 4);
            this.Decrease.Name = "Decrease";
            this.Decrease.Size = new System.Drawing.Size(31, 29);
            this.Decrease.TabIndex = 1;
            this.Decrease.UseVisualStyleBackColor = false;
            this.Decrease.Click += new System.EventHandler(this.Decrease_Click);
            // 
            // Increase
            // 
            this.Increase.BackColor = System.Drawing.Color.Transparent;
            this.Increase.FlatAppearance.BorderSize = 0;
            this.Increase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Increase.Location = new System.Drawing.Point(455, 4);
            this.Increase.Name = "Increase";
            this.Increase.Size = new System.Drawing.Size(36, 29);
            this.Increase.TabIndex = 4;
            this.Increase.UseVisualStyleBackColor = false;
            this.Increase.Click += new System.EventHandler(this.Increase_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(272, 56);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(229, 222);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Pause
            // 
            this.Pause.BackColor = System.Drawing.Color.Gainsboro;
            this.Pause.FlatAppearance.BorderSize = 0;
            this.Pause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Pause.ForeColor = System.Drawing.Color.Black;
            this.Pause.Location = new System.Drawing.Point(162, 3);
            this.Pause.Name = "Pause";
            this.Pause.Size = new System.Drawing.Size(34, 29);
            this.Pause.TabIndex = 2;
            this.Pause.UseVisualStyleBackColor = false;
            this.Pause.Click += new System.EventHandler(this.Pause_Click);
            // 
            // Play
            // 
            this.Play.BackColor = System.Drawing.Color.Transparent;
            this.Play.FlatAppearance.BorderSize = 0;
            this.Play.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Play.Location = new System.Drawing.Point(312, 4);
            this.Play.Name = "Play";
            this.Play.Size = new System.Drawing.Size(31, 29);
            this.Play.TabIndex = 3;
            this.Play.UseVisualStyleBackColor = false;
            this.Play.Click += new System.EventHandler(this.Play_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(0, 97);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(271, 148);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(502, 97);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(271, 148);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 36);
            this.button1.TabIndex = 7;
            this.button1.Text = "Damage Inspection";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(123, 1);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(90, 36);
            this.button2.TabIndex = 8;
            this.button2.Text = "OCR";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(212, 1);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(113, 36);
            this.button3.TabIndex = 9;
            this.button3.Text = "Empty Inspection";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(324, 1);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(113, 36);
            this.button4.TabIndex = 10;
            this.button4.Text = "Seal Check";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(436, 1);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(113, 36);
            this.button5.TabIndex = 11;
            this.button5.Text = "Genset Check";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(548, 1);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(113, 36);
            this.button6.TabIndex = 12;
            this.button6.Text = "Hazards";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(660, 1);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(113, 36);
            this.button7.TabIndex = 13;
            this.button7.Text = "Ingate";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // Seal
            // 
            this.Seal.AutoSize = true;
            this.Seal.Location = new System.Drawing.Point(4, 331);
            this.Seal.Name = "Seal";
            this.Seal.Size = new System.Drawing.Size(112, 17);
            this.Seal.TabIndex = 14;
            this.Seal.Text = "Seals Present (F1)";
            this.Seal.UseVisualStyleBackColor = true;
            // 
            // Hazard
            // 
            this.Hazard.AutoSize = true;
            this.Hazard.Location = new System.Drawing.Point(144, 331);
            this.Hazard.Name = "Hazard";
            this.Hazard.Size = new System.Drawing.Size(86, 17);
            this.Hazard.TabIndex = 15;
            this.Hazard.Text = "Hazards (F2)";
            this.Hazard.UseVisualStyleBackColor = true;
            // 
            // Damage
            // 
            this.Damage.AutoSize = true;
            this.Damage.Location = new System.Drawing.Point(258, 331);
            this.Damage.Name = "Damage";
            this.Damage.Size = new System.Drawing.Size(120, 17);
            this.Damage.TabIndex = 16;
            this.Damage.Text = "Damage Found (F3)";
            this.Damage.UseVisualStyleBackColor = true;
            // 
            // Notes
            // 
            this.Notes.Location = new System.Drawing.Point(4, 355);
            this.Notes.Name = "Notes";
            this.Notes.Size = new System.Drawing.Size(736, 20);
            this.Notes.TabIndex = 17;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Pause);
            this.panel1.Controls.Add(this.Decrease);
            this.panel1.Controls.Add(this.Play);
            this.panel1.Controls.Add(this.Increase);
            this.panel1.Location = new System.Drawing.Point(155, 381);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(497, 47);
            this.panel1.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(349, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Increase Speed (F9)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(262, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Play (F8)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(198, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Pause (F7)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Decrease Speed (F6)";
            // 
            // Complete
            // 
            this.Complete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Complete.FlatAppearance.BorderSize = 0;
            this.Complete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Complete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Complete.ForeColor = System.Drawing.Color.Transparent;
            this.Complete.Location = new System.Drawing.Point(4, 381);
            this.Complete.Name = "Complete";
            this.Complete.Size = new System.Drawing.Size(130, 33);
            this.Complete.TabIndex = 20;
            this.Complete.Text = "Complete (F10)";
            this.Complete.UseVisualStyleBackColor = false;
            this.Complete.Click += new System.EventHandler(this.Complete_Click);
            // 
            // ErrorMessage
            // 
            this.ErrorMessage.AutoSize = true;
            this.ErrorMessage.Location = new System.Drawing.Point(12, 442);
            this.ErrorMessage.Name = "ErrorMessage";
            this.ErrorMessage.Size = new System.Drawing.Size(0, 13);
            this.ErrorMessage.TabIndex = 21;
            // 
            // exit
            // 
            this.exit.BackColor = System.Drawing.Color.Transparent;
            this.exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.exit.FlatAppearance.BorderSize = 0;
            this.exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exit.Location = new System.Drawing.Point(669, 445);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(24, 19);
            this.exit.TabIndex = 9;
            this.exit.UseVisualStyleBackColor = false;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(689, 451);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Break (Ctrl+Tab)";
            // 
            // Record
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 464);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.ErrorMessage);
            this.Controls.Add(this.Complete);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Notes);
            this.Controls.Add(this.Damage);
            this.Controls.Add(this.Hazard);
            this.Controls.Add(this.Seal);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Record";
            this.Text = "Records";
            this.Load += new System.EventHandler(this.Record_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Record_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button Pause;
        private System.Windows.Forms.Button Play;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.CheckBox Seal;
        private System.Windows.Forms.CheckBox Hazard;
        private System.Windows.Forms.CheckBox Damage;
        private System.Windows.Forms.TextBox Notes;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Complete;
        private System.Windows.Forms.Button Decrease;
        private System.Windows.Forms.Button Increase;
        private System.Windows.Forms.Label ErrorMessage;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.Label label5;
    }
}

