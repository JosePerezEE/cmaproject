﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.Windows.Forms;
using ProyectoCMA.Services;
using ProyectoCMA.Models;

namespace ProyectoCMA
{
    public partial class Record : Form
    {
        //Declaration of variables

        //variable used to get an array of files in a specific folder
        private string[] fi;
        //variables Back, current and front are used to display images in a specific way
        private int back = -1;
        private int current = 0;
        private int front = 1;
        //TimerRunning is to control the speed the images are being displayed.
        //It is also used to stop and pause the animation.
        private Boolean timerRunning = true;
        //Variable needed to use the authentication service.
        private readonly AuthService _authService;
        //Variable needed to use the created service of the records and store the information in the database.
        private readonly RecordService recordService = new RecordService();
        private int userId;

        public Record(AuthService authService)
        {
            InitializeComponent();
            ButtonImages();
            Resizing();
            //KeyPreview is used to allow the user to use hotkeys.
            this.KeyPreview = true;
            _authService = authService;
            //Array of the files present in the Images folder inside the solution.
            fi = Directory.GetFiles(@"..\..\Images");
            //Use of a timer to control the images.
            timer1.Interval = 2000;
            timer1.Start();
            //Extracting the user ID with the authentication service.
            //Also needed to store, so it is possible to store the ID in the database.
            userId = _authService.GetUserId();

        }

        //Method used to display an image in certain buttons.
        //images present in the Symbols folder inside the solution.
        private void ButtonImages()
        {
            Pause.Image = Image.FromFile(@"..\..\Symbols\Pause.png");
            Play.Image = Image.FromFile(@"..\..\Symbols\Play.png");
            Decrease.Image = Image.FromFile(@"..\..\Symbols\Rewind.png");
            Increase.Image = Image.FromFile(@"..\..\Symbols\Forward.png");
            exit.Image = Image.FromFile(@"..\..\Symbols\breakV2.png");
        }

        //Method used to prevent users from resizing the form.
        //It is also used to make the form appear in the middle of the screen.
        private void Resizing()
        {
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void Record_Load(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //this code will only run IF there is atleast one image inside the folder.
            if (fi.Length > 0)
            {
                //Going through the arrays next to the Timer to present the images in order.
                back = (back >= fi.Length - 1) ? 0 : ++back;
                current = (current >= fi.Length - 1) ? 0 : ++current;
                front = (front >= fi.Length - 1) ? 0 : ++front;

                //Set the images inside the pictureboxes with the arrays displayed above.
                pictureBox1.Image = new Bitmap(fi[back]);
                pictureBox2.Image = new Bitmap(fi[current]);
                pictureBox3.Image = new Bitmap(fi[front]);

                //Refreshing the pictureboxes to update the current images going through the arrays.
                //Location and size are set to make it look like it that the middle image is zooming in and out.
                pictureBox1.Location = new Point(272, 97);
                pictureBox1.Size = new System.Drawing.Size(229, 148);
                pictureBox1.Refresh();
                pictureBox1.Location = new Point(272, 56);
                pictureBox1.Size = new System.Drawing.Size(229, 222);
                pictureBox2.Refresh();
                pictureBox3.Refresh();
            }
        }

        //Stopping the Timer Running variable initialized at the beginning to make the images stop moving.
        private void Pause_Click(object sender, EventArgs e)
        {
            if (timerRunning)
            {
                timer1.Stop();
                timerRunning = false;
            }
        }

        //Restarting the timer to make the images play again after a pause.
        private void Play_Click(object sender, EventArgs e)
        {

                timer1.Start();
                timerRunning = true;
        }

        //Button used to decrease the speed of the ongoing images in the form.
        private void Decrease_Click(object sender, EventArgs e)
        {
            //Setting an integer variable.
            int a = 100;
            //Make it so the time interval doesn't surpass 10000.
            //This is for security and performance issues
            if (timer1.Interval == 10000)
            {
                timer1.Interval = timer1.Interval;
            }
            else
            {
                timer1.Interval = timer1.Interval + a;

            }
        }

        //Button used to increase the speed of the ongoing images in the form.
        private void Increase_Click(object sender, EventArgs e)
        {
            //Setting an integer variable.
            int a = 100;
            //Making it so the time interval do not fall below 500.
            //This is for security and performance issues.
            if (timer1.Interval == 500)
            {
                timer1.Interval = timer1.Interval;
            }
            else
            {
                timer1.Interval = timer1.Interval - a;

            }
        }

        //Button used to store the specified records in the database.
        private async void Complete_Click(object sender, EventArgs e)
        {
            //Using the model and the textboxes inside the form to request a response from the API.
            //This is the main way of communication the program uses to call the API.
            var record = new RecordModel
            {
                UserId = userId,
                Seal = this.Seal.Checked,
                Hazards = this.Hazard.Checked,
                DamagesFound = this.Damage.Checked,
                Note = this.Notes.Text
            };
            //Sending the information collected in the form through the API
            var result = await this.recordService.CreateRecord(record);
            //Validating the response from the created service.
            //If it is successful, a label will show that the information has been successfully stored.
            //If not, it will throw an error.
            if (result.IsCreated)
            {
                this.ErrorMessage.Text = result.ErrorMessage;

            }
            else
            {
                this.ErrorMessage.Text = result.ErrorMessage;
            }
            //This method is used to clear the textbox and checkboxes after each request.
            this.ClearForm();
        }

        //Method created to clean the initial input from the user.
        public void ClearForm()
        {
            this.Hazard.Checked = false;
            this.Seal.Checked = false;
            this.Damage.Checked = false;
            this.Notes.Text = string.Empty;
        }

        //KeyDown is used to implement the use of hotkeys in the form.
        private void Record_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.F6)
            {
                Decrease.PerformClick();
            }
            if (e.KeyCode == Keys.F7)
            {
                Pause.PerformClick();
            }
            if (e.KeyCode == Keys.F8)
            {
                Play.PerformClick();
            }
            if (e.KeyCode == Keys.F9)
            {
                Increase.PerformClick();
            }
            if (e.KeyCode == Keys.F10)
            {
                Complete.PerformClick();
            }
            if (e.Control == true && e.KeyCode == Keys.Tab)
            {
                exit.PerformClick();
            }
            if (e.KeyCode == Keys.F1)
            {
                if(Seal.Checked == true)
                {
                    this.Seal.Checked = false;
                }
                else
                {
                    this.Seal.Checked = true;
                }
            }
            if (e.KeyCode == Keys.F2)
            {
                if (Hazard.Checked == true)
                {
                    this.Hazard.Checked = false;
                }
                else
                {
                    this.Hazard.Checked = true;
                }
            }
            if (e.KeyCode == Keys.F3)
            {
                if (Damage.Checked == true)
                {
                    this.Damage.Checked = false;
                }
                else
                {
                    this.Damage.Checked = true;
                }
            }
        }

        //Main method used to exit the main program and return to the login form.
        private void exit_Click(object sender, EventArgs e)
        {
            LoginForm f1 = new LoginForm();
            this.Close();
            f1.Show();
        }
    }
}
