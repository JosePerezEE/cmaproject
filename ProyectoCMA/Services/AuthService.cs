﻿using Newtonsoft.Json;
using ProyectoCMA.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoCMA.Services
{
    //One of the main service used to communicate with the API.
    //LOGIN PROCESS
    public class AuthService
    {
        private static readonly HttpClient client = new HttpClient();
        private string token;
        public async Task<LoginResponse> Login(User user)
        {
            //Using the Serialization method to convert the JSON.
            var result = new LoginResponse();
            var json = JsonConvert.SerializeObject(user);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            //Awaiting a response from the API.
            var response = await client.PostAsync("https://localhost:44335/api/auth/login", content);
            result.ErrorMessage = await response.Content.ReadAsStringAsync();
            //validating the response and sending a bool along with a message to specify that the process has been successful or if it has failed.
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var tokenModel = JsonConvert.DeserializeObject<TokenModel>(result.ErrorMessage);
                this.token = tokenModel.Token;

                result.IsLogged = true;
                result.ErrorMessage = string.Empty;
            }
            else
            {
                result.IsLogged = false;
            }
            
            return result;
        }

        //The use of the token to extract the main ID of an specific user, so that it can be stored along with the main records.
        //It is using claims to see through the information described in the token.
        public int GetUserId()
        {
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(this.token);
            var tokenS = handler.ReadToken(this.token) as JwtSecurityToken;

            return Convert.ToInt32(tokenS.Claims.First(claim => claim.Type.Equals("nameid")).Value);
        }
    }
}
