﻿using Newtonsoft.Json;
using ProyectoCMA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoCMA.Services
{
    //One of the main service used to communicate with the API.
    //STORING TO DATABASE PROCESS
    public class RecordService
    {
        private static readonly HttpClient client = new HttpClient();
        public async Task<RecordResponse> CreateRecord(RecordModel record)
        {
            //Using the Serialization method to convert the JSON.
            var result = new RecordResponse();
            var json = JsonConvert.SerializeObject(record);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            //Awaiting a response from the API.
            var response = await client.PostAsync("https://localhost:44335/api/records", content);
            result.ErrorMessage = await response.Content.ReadAsStringAsync();
            //validating the response and sending a bool along with a message to specify that the process has been successful or if it has failed.
            //This part is mainly used to notify the user about the process.
            if (response.StatusCode == System.Net.HttpStatusCode.Created)
            {
                result.IsCreated = true;
                result.ErrorMessage = "Data successfully added to the database.";
            }
            else
            {
                result.IsCreated = false;
            }
            return result;
        }
    }
}
