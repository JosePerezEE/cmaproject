﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoCMA.Models
{
    //Model with the specific records to be used in the storing process.
    //It is used to communicate with the API and the main program.
    public class RecordModel
    {
        public int UserId { get; set; }
        public bool Seal { get; set; }
        public bool Hazards { get; set; }
        public bool DamagesFound { get; set; }
        public string Note { get; set; }
    }
}
