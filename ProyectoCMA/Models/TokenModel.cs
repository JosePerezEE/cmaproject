﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoCMA.Models
{
    //Token model used to validate the information that comes through the token.
    //It is also used in the login process to extract the ID of the user.
    public class TokenModel
    {
        public string Token { get; set; }
    }
}
