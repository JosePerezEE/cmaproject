﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoCMA.Models
{
    //Model used to manage the responses from the API login process.
    public class LoginResponse
    {
        public bool IsLogged { get; set; }
        public string ErrorMessage { get; set; }
    }
}
