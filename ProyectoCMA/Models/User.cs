﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoCMA.Models
{
    //Model to be used in the login process though the API.
    //It is used to communicate with the API and the main program.
    public class User
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
