﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoCMA.Models
{
    //Model used to manage the responses from the storage process to database used thoughout an API.

    public class RecordResponse
    {
        public bool IsCreated { get; set; }
        public string ErrorMessage { get; set; }
    }
}
