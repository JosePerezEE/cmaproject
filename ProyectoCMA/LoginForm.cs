﻿using ProyectoCMA.Models;
using ProyectoCMA.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoCMA
{
    public partial class LoginForm : Form
    {
        //Declaring a variable to use the authentication service that is being used in the API.
        private AuthService authService = new AuthService();

        public LoginForm()
        {
            InitializeComponent();
            Resizing();
        }

        //Method used to prevent users from resizing the form.
        //It is also used to make the form appear in the middle of the screen.
        private void Resizing()
        {
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private async void login_Click(object sender, EventArgs e)
        {
            //Using the model and the textboxes inside the form to request a response from the API.
            //This is the main way of communication the program uses to call the API.
            var user = new User { UserName = this.Username.Text, Password = this.Password.Text };
            var response = await this.authService.Login(user);
            //Validating the response from the created service.
            //If it is successful, the program will open. If not, it will throw an error in a label.
            if(response.IsLogged)
            {
                Record f1 = new Record(this.authService);
                f1.Show();
                this.Hide();
            }
            else
            {
                this.Message.Text = response.ErrorMessage;
            }
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {

        }
    }
}
