# CMAProject
Connection string to be used needs to be set before running the program.

- Go to **appsettings.json** found in the **Project_API** program.
- Change the ServerName to your database connection string:

"CMA": "Server=ServerName\\SQLEXPRESS;Database=CMACGM;Trusted_Connection=True;"